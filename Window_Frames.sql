WITH WeeklySales AS (
    SELECT
        sh.times.calendar_week_number,
        sh.times.day_number_in_week,
        sh.sales s.amount_sold,
        SUM(a,amount_sold) OVER (PARTITION BY day_number_in_week ORDER BY day_of_week) AS CUM_SUM
    FROM
        sh.sales s
    WHERE
        sh.times.day_number_in_week IN (49, 50, 51)
)

SELECT
    ws.calendar_week_number,
    ws.day_of_week,
    ws.sales_amount,
    ws.CUM_SUM,
    AVG(ws.amount_sold) OVER w AS CENTERED_3_DAY_AVG
FROM
    WeeklySales ws
WINDOW w AS (
    ORDER BY
        ws.calendar_week_number,
        CASE
            WHEN ws.day_of_week = 'Monday' THEN 1
            WHEN ws.day_of_week = 'Tuesday' THEN 2
            WHEN ws.day_of_week = 'Wednesday' THEN 3
            WHEN ws.day_of_week = 'Thursday' THEN 4
            WHEN ws.day_of_week = 'Friday' THEN 5
            WHEN ws.day_of_week = 'Saturday' THEN 6
            WHEN ws.day_of_week = 'Sunday' THEN 7
        END
)
ORDER BY
    ws.calendar_week_number,
    ws.day_of_week;
